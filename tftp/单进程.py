# from socket import *
# serSocket = socket(AF_INET, SOCK_STREAM)
# localAddr = ('192.168.34.135',7789)
# serSocket.bind(localAddr)
# serSocket.listen(5)
# while True:
#     print("主进程等待新客户端")
#     newSocket,destAddr = serSocket.accept()
#     print("主进程接下来负责处理",str(destAddr))
#     try:
#         while True:
#             recvData = newSocket.recv(1024)
#             if len(recvData)>0: #如果收到的客户端数据长度为0，代表客户端已经调用close（）下线
#                 print("接收到", str(destAddr),recvData)
#             else:
#                 print("%s-客户端已关闭" %str(destAddr))
#                 break
#     finally:
#         newSocket.close()
# serSocket.close()

from socket import *
server = socket(AF_INET,SOCK_STREAM)     #创建套接字，设置为套接字只能使用一个网络端口
server_addr = ("192.168.34.135",8080)     #设置服务器的IP和网络端口号
server.bind(server_addr)                 #绑定服务器的IP和网络端口号
#先设置最大连接数之后的等待连接数
server.listen(5)    #这个数是服务器测试之后得出的等待链接数
while True:
    print("主进程等待新的客户端")
    new_socket,user_ip = server.accept()     #对访问的客户端创建新的套接字,并且得到客户端的IP地址
    print(f"访问者为IP{str(user_ip)}的客户端")

    try:
        while True:                      #服务器不知道客户端要发送多少数据，因此为死循环接收
            user_date = new_socket.recv(1024)        #设置一次要接受的最大字节数
            if len(user_date) > 0:                   #判断要接收到的数据是否小于0，大于0，说明没有发送完，需要继续接收
                print(f"接收到来自{str(user_ip)}的{len(user_date)}数据")   #提示接收到的信息
                print(user_date.decode())             #显示接收到客户端的数据
                new_socket.send("我已收到".encode())

            else:
                print(f"{str(user_ip)}客户端已经下线")    #如果发送的字节小于0，客户端就下线了
                break
    except Exception:
        print('远程主机强迫关闭了一个现有的连接')
    finally:
        new_socket.close()
