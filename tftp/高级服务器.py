import socketserver
# 自定义类来实现通信循环
class MyServer(socketserver.BaseRequestHandler):
    # 必须写入handle方法，建立链接时会自动执行handle方法
    def handle(self):
        while True:
            data = self.request.recv(1024)
            # handle 方法通过属性 self.request 来访问客户端套接字
            print('->client:', data)
            self.request.send(data.upper())

socketserver.TCPServer.allow_reuse_address = True
server = socketserver.ThreadingTCPServer(('192.168.34.135', 8080), MyServer)
server.serve_forever()
