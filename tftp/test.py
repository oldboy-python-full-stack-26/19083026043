from socket import *
from subprocess import *
server = socket(AF_INET,SOCK_STREAM)
server.bind(("127.0.0.1",1314))
server.listen(5)
while True:
    new_server,client_addr = server.accept()
    print("客户端的IP:",client_addr)
    while True:
        try:
            cmd = new_server.recv(1024)
            ret = Popen(cmd.decode('utf-8'),shell=True,stdout=PIPE,stderr=PIPE)
            right_msg = ret.stdout.read()#读出正确信息
            woring_msg = ret.stderr.read()#读出错误信息
            new_server.send(right_msg + woring_msg)
        except ConnectionResetError:
            print("服务结束")
            break
    new_server.close()
server.close()

