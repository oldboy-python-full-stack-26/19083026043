# from socket import *
# tcpSerSocket = socket(AF_INET, SOCK_STREAM)
# address = ("", 7788)
# tcpSerSocket.bind(address)
# tcpSerSocket.listen(5)#设置最大连接数
# newSocket, clientAddr = tcpSerSocket.accept()
# # 如果有新的客户端来链接服务器， 那么就产⽣⼀个新的套接字
# # newSocket⽤来为这个客户端服务（10086小妹）
# # tcpSerSocket就可以省下来等待其他新客户端的链接
# # 接收对⽅发送过来的数据， 最⼤接收1024个字节
# recvData = newSocket.recv(1024) #接收tcp数据
# # 发送⼀些数据到客户端
# newSocket.send('thank you !')   #发送tcp数据
# # 关闭为这个客户端服务的套接字， 只要关闭了， 就意味着为不能再为这个客户端服务了
# newSocket.close()
# # 关闭监听套接字， 只要这个套接字关闭了， 就意味着整个程序不能再接收任何新的客户端的连接
# tcpSerSocket.close()
from socket import *

s = socket(AF_INET,SOCK_STREAM)
s.bind(('127.0.0.1',8088))

s.listen(5)
while True:
    news, addr = s.accept()
    print(news)
    news.send("你好".encode())
    data = news.recv(1024)
    print(data.decode())
    news.close()
s.close()


