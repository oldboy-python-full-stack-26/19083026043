# work35

 1.查询egon写的所有书和价格

select name ,price from book where author ='egon';

2.找出最贵的图书的价格

select max(price) from book;

3.求所有图书的均价

select avg(price)  from book;

 avg(price) |
+------------+
|   34.57143 |

4.将所有图书按照出版日期排序

select * from book order by pub_date;

+-----------------+--------+--------------------------------+-------+------------+
| name            | author | Press                          | price | pub_date   |
+-----------------+--------+--------------------------------+-------+------------+
| 九阴真经        | yuan   | 北京工业地雷出版社             |  62.0 | 2017-07-12 |
| 独孤九剑        | alex   | 北京工业地雷出版社             |  12.0 | 2017-09-01 |
| 九阳神功        | alex   | 人民音乐不好听出版社           |   5.0 | 2018-07-04 |
| 倚天屠龙记      | egon   | 北京工业地雷出版社             |  70.0 | 2019-07-01 |
| 降龙十巴掌      | egon   | 知识产权没有用出版社           |  20.0 | 2019-07-05 |
| 葵花宝典        | yuan   | 知识产权没有用出版社           |  33.0 | 2019-08-02 |
| 九阴白骨爪      | jinxin | 人民音乐不好听出版社           |  40.0 | 2019-08-07 |
+-----------------+--------+--------------------------------+-------+------------+

5.查询alex写的所有书的平均价格

select author,avg(price) from book where author ='alex';

+--------+------------+
| author | avg(price) |
+--------+------------+
| alex   |    8.50000 |
+--------+------------+

6.查询人民音乐不好听出版社出版的所有图书

select * from book where press='人民音乐不好听出版社';

+-----------------+--------+--------------------------------+-------+------------+
| name            | author | Press                          | price | pub_date   |
+-----------------+--------+--------------------------------+-------+------------+
| 九阳神功        | alex   | 人民音乐不好听出版社           |   5.0 | 2018-07-04 |
| 九阴白骨爪      | jinxin | 人民音乐不好听出版社           |  40.0 | 2019-08-07 |
+-----------------+--------+--------------------------------+-------+------------+

7.查询人民音乐出版社出版的alex写的所有图书和价格

select name,price from book where author ='alex' and press ='人民音乐不好听出版社';

+--------------+-------+
| name         | price |
+--------------+-------+
| 九阳神功     |   5.0 |
+--------------+-------+

8.找出出版图书均价最高的作者

 select author 作者, avg(price) 价格 from book
    -> group by author
    -> order by avg(price) desc limit 1;
+--------+----------+
| 作者   | 价格     |
+--------+----------+
| yuan   | 47.50000 |
+--------+----------+

9.找出最新出版的图书的作者和出版社

 select name,author,press,pub_date from book
    -> order by pub_date desc limit 1;
+-----------------+--------+--------------------------------+------------+
| name            | author | press                          | pub_date   |
+-----------------+--------+--------------------------------+------------+
| 九阴白骨爪      | jinxin | 人民音乐不好听出版社           | 2019-08-07 |
+-----------------+--------+--------------------------------+------------+

10.显示各出版社出版的所有图书

select press,group_concat(name) from book group by press;

+--------------------------------+-------------------------------------------+
| press                          | group_concat(name)                        |
+--------------------------------+-------------------------------------------+
| 人民音乐不好听出版社           | 九阳神功,九阴白骨爪                       |
| 北京工业地雷出版社             | 倚天屠龙记,九阴真经,独孤九剑              |
| 知识产权没有用出版社           | 降龙十巴掌,葵花宝典                       |
+--------------------------------+-------------------------------------------+

11.查找价格最高的图书，并将它的价格修改为50元

update  book set price=50 order by price desc limit 1;

+-----------------+--------+--------------------------------+-------+------------+
| name            | author | Press                          | price | pub_date   |
+-----------------+--------+--------------------------------+-------+------------+
| 倚天屠龙记      | egon   | 北京工业地雷出版社             |  50.0 | 2019-07-01

12.删除价格最低的那本书对应的数据

delete from book order by price limit 1;

-----------------+--------+--------------------------------+-------+------------+
| name            | author | Press                          | price | pub_date   |
+-----------------+--------+--------------------------------+-------+------------+
| 倚天屠龙记      | egon   | 北京工业地雷出版社             |  50.0 | 2019-07-01 |
| 九阴真经        | yuan   | 北京工业地雷出版社             |  62.0 | 2017-07-12 |
| 九阴白骨爪      | jinxin | 人民音乐不好听出版社           |  40.0 | 2019-08-07 |
| 独孤九剑        | alex   | 北京工业地雷出版社             |  12.0 | 2017-09-01 |
| 降龙十巴掌      | egon   | 知识产权没有用出版社           |  20.0 | 2019-07-05 |
| 葵花宝典        | yuan   | 知识产权没有用出版社           |  33.0 | 2019-08-02 |
+-----------------+--------+--------------------------------+-------+------------+

13.将所有alex写的书作业修改成alexsb

update book set author='alexsb' where author='alex';

+-----------------+--------+--------------------------------+-------+------------+
| name            | author | Press                          | price | pub_date   |
+-----------------+--------+--------------------------------+-------+------------+
| 倚天屠龙记      | egon   | 北京工业地雷出版社             |  50.0 | 2019-07-01 |
| 九阴真经        | yuan   | 北京工业地雷出版社             |  62.0 | 2017-07-12 |
| 九阴白骨爪      | jinxin | 人民音乐不好听出版社           |  40.0 | 2019-08-07 |
| 独孤九剑        | alexsb | 北京工业地雷出版社             |  12.0 | 2017-09-01 |
| 降龙十巴掌      | egon   | 知识产权没有用出版社           |  20.0 | 2019-07-05 |
| 葵花宝典        | yuan   | 知识产权没有用出版社           |  33.0 | 2019-08-02 |
+-----------------+--------+--------------------------------+-------+------------+



14.select year(publish_date) from book

自己研究上面sql语句中的year函数的功能，完成需求：
将所有2017年出版的图书从数据库中删除

delete from book where year(pub_date)= '2017';

-----------------+--------+--------------------------------+-------+------------+
| name            | author | Press                          | price | pub_date   |
+-----------------+--------+--------------------------------+-------+------------+
| 倚天屠龙记      | egon   | 北京工业地雷出版社             |  50.0 | 2019-07-01 |
| 九阴白骨爪      | jinxin | 人民音乐不好听出版社           |  40.0 | 2019-08-07 |
| 降龙十巴掌      | egon   | 知识产权没有用出版社           |  20.0 | 2019-07-05 |
| 葵花宝典        | yuan   | 知识产权没有用出版社           |  33.0 | 2019-08-02 |
+-----------------+--------+--------------------------------+-------+------------+

15.有文件如下，请根据[链接](https://www.cnblogs.com/Eva-J/articles/9772614.html)自学pymysql模块，使用python写代码将文件中的数据写入数据库
学python从开始到放弃|alex|人民大学出版社|50|2018-7-1
学mysql从开始到放弃|egon|机械工业出版社|60|2018-6-3
学html从开始到放弃|alex|机械工业出版社|20|2018-4-1
学css从开始到放弃|wusir|机械工业出版社|120|2018-5-2
学js从开始到放弃|wusir|机械工业出版社|100|2018-7-30 

name                        | author | Press                          | price | pub_date   |
+-----------------------------+--------+--------------------------------+-------+------------+
| 倚天屠龙记                  | egon   | 北京工业地雷出版社             |  50.0 | 2019-07-01 |
| 九阴白骨爪                  | jinxin | 人民音乐不好听出版社           |  40.0 | 2019-08-07 |
| 降龙十巴掌                  | egon   | 知识产权没有用出版社           |  20.0 | 2019-07-05 |
| 葵花宝典                    | yuan   | 知识产权没有用出版社           |  33.0 | 2019-08-02 |
| 学python从开始到放弃        | alex   | 人民大学出版社                 |  50.0 | 2018-07-01 |
| 学mysql从开始到放弃         | egon   | 机械工业出版社                 |  60.0 | 2018-06-03 |
| 学html从开始到放弃          | alex   | 机械工业出版社                 |  20.0 | 2018-04-01 |
| 学css从开始到放弃           | wusir  | 机械工业出版社                 | 120.0 | 2018-05-02 |
| 学js从开始到放弃            | wusir  | 机械工业出版社                 | 100.0 | 2018-07-30 |
+-----------------------------+--------+--------------------------------+-------+------------+



import pymysql

db = pymysql.connect("127.0.0.1", "root", "123", "db1")

使用cursor()方法获取操作游标

cursor = db.cursor()

SQL 插入语句

sql = """INSERT INTO BOOK(NAME,
         AUTHOR, PRESS, PRICE, PUB_DATE)
         VALUES ('学python从开始到放弃', 'alex', '人民大学出版社', 50, '2018-7-1'),
         ('学mysql从开始到放弃', 'egon', '机械工业出版社', 60, '2018-6-3'),
         ('学html从开始到放弃', 'alex', '机械工业出版社', 20, '2018-4-1'),
         ('学css从开始到放弃', 'wusir', '机械工业出版社', 120, '2018-5-2'),
         ('学js从开始到放弃', 'wusir', '机械工业出版社', 100, '2018-7-30')"""
try:
    cursor.execute(sql)  # 执行sql语句
    db.commit()  # 提交到数据库执行CC
except:
    db.rollback()  # 如果发生错误则回滚

关闭数据库连接

db.close()