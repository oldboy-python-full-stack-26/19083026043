# 1.
# 有如下
# v1 = {'郭宝元', 'alex', '海绵', '王二麻子'}
# v2 = {'alex', '王二麻子'}
# 请得到v1和 v2的交集并输出
# print(v1 & v2)
# 请得到v1和v2的并集并输出
# print(v1 | v2)
# 请得到v1和v2的差集并输出
# print(v1 - v2)
# 请得到v2和v1的差集并输出
# print(v1 + v2)
# 2.
# 循环提示用户输入，并将输入内容追加到列表中（如果输入N或n则停止循环）
# lst = []
# while True:
#     content = input("请输入内容：")
#     if content.upper() == "N":
#         break
#     lst.append(content)
# print(lst)

# 3.
# 写代码实现
# v1 = {'alex', '武sir', '黑哥'}
# v2 = []
# ​
# 循环提示用户输入，如果输入的内容在v1中存在，则追加到v2中，如果v1中不存在，则添加到v1中。（如果输入N或n则停止循环）

# v1 = {'alex','武sir','黑哥'}
# v2 = []
# while True:
#     content = input("请输入：")
#     if content.upper() == "N":
#         break
#     if content in v1:
#         v2.append(content)
#     else:
#         v1.add(content)
# print(v1,v2)

# 4.
# 通过观察判断以下值那个能做字典的key？那个能做集合的元素？
# 1  # 能做字典的key，能做集合的元素
# -1  # 能做字典的key，能做集合的元素
# ""    # 能做字典的key，能做集合的元素
# None    # 能做字典的key，能做集合的元素
# [1, 2]
# (1,)  # 能做字典的key，能做集合的元素
# {11, 22, 33, 4}
# {'name': 'wupeiq', 'age': 18}
#
# 5. is 和 == 的区别？
# == 是判断值是否相等
# is 是判断内存地址是否相同
# 6.
# type使用方式及作用？
#查看数据类型
# 7.
# id的使用方式及作用？
#查看内存地址
# 8.
# 看代码写结果并解释原因
#
# v1 = {'k1': 'v1', 'k2': [1, 2, 3]}
# v2 = {'k1': 'v1', 'k2': [1, 2, 3]}
# ​
# result1 = v1 == v2
# result2 = v1 is v2
# print(result1)
# print(result2)
# print(result1)  # True   ==两边内容一样,正确
# print(result2)  # False   两边内存地址不同

# 9.
# 看代码写结果并解释原因
#
# v1 = {'k1': 'v1', 'k2': [1, 2, 3]}
# v2 = v1
# ​
# result1 = v1 == v2
# result2 = v1 is v2
# print(result1) #True
# print(result2) #True
# 10.
# 看代码写结果并解释原因
#
# v1 = {'k1': 'v1', 'k2': [1, 2, 3]}
# v2 = v1
# ​
# v1['k1'] = 'meet' #可变数据类型原地修改
# print(v2)
# 正确
# 11.
# 看代码写结果并解释原因
#
# v1 = '人生苦短，我用Python'
# v2 = [1, 2, 3, 4, v1]
# v1 = "人生苦短，用毛线Python" #字符串不可变数据类型，修改时需新开辟空间
# print(v2)
# 正确
# 12.
# 看代码写结果并解释原因
#
# info = [1, 2, 3]
# userinfo = {'account': info, 'num': info, 'money': info}
# ​
# info.append(9) #可变数据类型原地修改
# print(userinfo) #{'account':[1,2,3,9], 'num':[1,2,3,9], 'money':[1,2,3,9]}
# ​
# info = "题怎么这么多" #数据类型改变，新开辟空间，新的指向，原来的数据不变
# print(userinfo) #{'account':[1,2,3,9], 'num':[1,2,3,9], 'money':[1,2,3,9]}
# 正确
# 13.
# 看代码写结果并解释原因
#
# info = [1, 2, 3]
# userinfo = [info, info, info, info, info]
# ​
# info[0] = '不仅多，还特么难呢' #可变数据类型，原地修改
# print(info, userinfo) #[不仅多，还特么难呢',2,3] [[不仅多，还特么难呢',2,3],[不仅多，还特么难呢',2,3],[不仅多，还特么难呢',2,3],[不仅多，还特么难呢',2,3],[不仅多，还特么难呢',2,3]]
# 正确
# 14.
# 看代码写结果并解释原因
#
# info = [1, 2, 3]
# userinfo = [info, info, info, info, info]
# ​
# userinfo[2][0] = '闭嘴' #可变数据原地修改，共用一个内存地址则同步进行修改
# print(info, userinfo)  #info = [闭嘴',2,3] [[闭嘴',2,3],[闭嘴',2,3],[闭嘴',2,3],[闭嘴',2,3],[闭嘴',2,3]]
# 15.
# 看代码写结果并解释原因
#
# info = [1, 2, 3]
# user_list = []
# for item in range(10): #10次
#     user_list.append(info)
#
# info[1] = "是谁说Python好学的？" #修改为[1,"是谁说Python好学的？",3]
# ​
# print(user_list) #[[1,"是谁说Python好学的？",3]*10]
# 正确
# 16.
# 看代码写结果并解释原因
#
# data = {}
# for i in range(10):
#     data['user'] = i #第一次增加，后面持续覆盖
# print(data) #{'user':9}
# 正确
# 17.
# 看代码写结果并解释原因
#
# data_list = []
# data = {}
# for i in range(10):
#     data['user'] = i #第一次增加,然后持续覆盖,最后为9
#     data_list.append(data) #每次追加data字典，共10次，可变数据类型每次原地修改
# print(data_list)  #[{'user':9}*10]
# 正确
# 18.
# 看代码写结果并解释原因
#
# data_list = []
# for i in range(10): #10次
#     data = {}
#     data['user'] = i
#     data_list.append(data) 每循环一次字典就会追加到列表
# print(data_list) #[{'user':0},{'user':1},……,{'user':9}]
# 正确
# 19.
# 使用循环打印出一下效果：
# 格式一
#
# *
# **
# ** *
# ** **
# ** ** *
# 格式二
#
# ** **
# ** *
# **
# *
# 格式三
#
# *
# ** *
# ** ** *
# ** ** ** *
# ** ** ** ** *
# 格式一
# for i in range(1,6):
#     print("*" * i)

# 格式二
# for i in range(4,0,-1):
#     print("*" * i)

# 格式三
# for i in range(6):
#     print("*" * (2 * i + 1))
# 20.
# 敲七游戏.从1开始数数.遇到7或者7的倍数（不包含17, 27, 这种数）要在桌上敲⼀下.编程来完成敲七.给出⼀个任意的数字n.从1开始数.数到n结束.把每个数字都放在列表中, 在数的过程中出现7或
# 者7的倍数（不包含17, 27, 这种数）.则向列表中添加⼀个
# '咣'
# 例如, 输⼊10.
# lst = [1, 2, 3, 4, 5, 6, '咣', 8, 9, 10]
# lst = []
# for i in range(1,int(input("请输入数字："))+1):
#     if i == 7 or i % 7 == 0:
#         lst.append("咣")
#         continue
#     lst.append(i)
# print(lst)

#
# 21.
# 模拟购物车
#
# 要求:
# 1, 用户先给自己的账户充钱：比如先充3000元。
# 2, 有如下的一个格式:
#
# goods = [{"name": "电脑", "price": 1999},
#          {"name": "鼠标", "price": 10},
#          {"name": "游艇", "price": 20},
#          {"name": "美女", "price": 998}, ]
# 3, 页面显示
# 序号 + 商品名称 + 商品价格，如：
# 1
# 电脑
# 1999
# 2
# 鼠标
# 10
# …
#
# 4, 用户输入选择的商品序号，然后打印商品名称及商品价格, 并将此商品，添加到购物车(自己定义购物车)，用户还可继续添加商品。
#
# 5, 如果用户输入的商品序号有误，则提示输入有误，并重新输入。
#
# 6, 用户输入N为购物车结算，依次显示用户购物车里面的商品，数量及单价，若充值的钱数不足，则让用户删除某商品，直至可以购买，若充值的钱数充足，则可以直接购买。
#
# 7, 用户输入Q或者q退出程序。
#
# 8, 退出程序之后，依次显示用户购买的商品，数量，单价，以及此次共消费多少钱，账户余额多少，并将购买信息显示。