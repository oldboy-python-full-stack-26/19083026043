# 1.判断下列逻辑语句的结果,一定要自己先分析 (3<4这种是一体)
# and数字进行逻辑运算时:
#     数字不为 0 时和不为 False
#           and运算选择and后边的内容
#           and运算都为假时选择and前的内容
#           and 运算一真一假选择假
# or 数字进行逻辑运算时:
#     数字不为 0 时和不为 False
#           or运算选择or前边的内容
#           or运算都为假时选择or后边的内容
#           or 运算一真一假选择真
# 1）1 > 1  or  3 < 4  or  4 > 5  and  2 > 1  and  9 > 8  or  7 < 6
#    F      or    T    or    F    and    T    and    T   or     F
#    F      or    T    or          F          and    T   or     F
#    F      or    T    or     F      or   F
#                     T

# 2）not 2 > 1 and 3 < 4 or 4 > 5 and 2 > 1 and 9 > 8 or 7 < 6
#       F     and   T    or   F   and     T   and      T   or    F
#              F         or        F          and      T   or    F
#              F         or                   F            or    F
#                            F

# 2.求出下列逻辑语句的值,一定要自己分析
# 1)8 or  3 and 4  or 2 and 0 or 9 and 7
#   8 or    4      or    0    or    7
#              8

# 2)0 or 2 and 3 and 4 or 6 and 0 or 3
#    0  or    4  or  0  or 3
#       4


# 3)1 and 0 or 8 and 9 and 5 or 2
#      0    or    5    or    2
#               5


# 4)4 or 8  and not False and 8 or 9
#   4 or 8  and   True   and  8 or 9
#   4 or    8  or  9
#      4   or   9
#           4

# 3.下列结果是什么? (2>1这种是一体)
# 6 or 2 > 1
# 6 or  T
#   6

# 3 or 2 > 1
# 3 or  T
#   3

# 0 or 5 < 4
# 0 or F
#   F


# 5 < 4 or 3
#  F    or  3
#       3

# 2 > 1 or 6
#   F   or 6
#       6

# 3 and 2 > 1
# 3 and   T
#     T

# 0 and 3 > 1
# 0 and  T
#    0

# 2 > 1 and 3
#  3

# 3 > 1 and 0
#   0

# 3 > 1 and 2 or 2 < 3 and 3 and 4 or 3 > 2
#        2    or    4   or  T
#               2

# 4.简述ASCII、Unicode、utf-8编码
"""
ASCII 不支持中文
a 一个字符占用8位（一字节）

Unicode
英文 四个字节 32位
英文 四个字节 32位

utf-8（最流行）
英文 一字节               8位
欧洲 二字节               16位
亚洲 一字节               32位
"""

# 5.简述位和字节的关系？
"""
1字节=8位
1bytes=8bit
1024bytes=1kb
"""

# 6.while循环语句基本结构？
"""
while 条件：
    循环体
"""
# 7.利用while语句写出猜大小的游戏：
# 设定一个理想数字比如：66，让用户输入数字，如果比66大，则显示猜测的结果大了；如果比66小，则显示猜测的结果小了;
# 只有等于66，显示猜测结果正确，然后退出循环。
while True:
    num=int(input("请输入数字:"))
    if num > 66:
        input("大了")
    elif num <66:
        print("小了")
    else:
        print("恭喜你,答对了")
        break


# 8.在7题的基础上进行升级：
# 给用户三次猜测机会，如果三次之内猜测对了，则显示猜测正确，退出循环，如果三次之内没有猜测正确，则自动退出循环，并显示‘太笨了你....’。
count = 0
while count < 3:
    num = int(input("请输入您的数字："))
    if num > 66:
        print("猜大了")
    elif num < 66:
        print("猜小了")
    else:
        print("恭喜您，猜对了")
        break
    count += 1
else:
    print("太笨了你....")


# 9.使用while循环输出 1 2 3 4 5 6 8 9 10
count = 0
while count < 10:
    count += 1
    print(count)


# 10.求1-100的所有数的和
count = 0
sum = 0
while count < 100:
    count += 1
    sum = sum+ count
print(sum)


# 11.输出 1-100 内的所有奇数
count = 1
while count < 100:
    print(count)
    count +=1


# 12.输出 1-100 内的所有偶数
count = 2
while count < 101:
    print(count)
    count +=2


# 13.求1-2+3-4+5 ... 99的所有数的和
i = 1
sum1 = 0
sum2 = 0
while i <= 100:
    if i % 2 == 0:
        sum1 += i
    else:
        sum2 += i
    i +=1
print(sum1-sum2)


# 14.⽤户登陆（三次输错机会）且每次输错误时显示剩余错误次数（提示：使⽤字符串格式化）
count = 3
while count >= 1:
    name = input("请输入用户名：")
    if name == "gg":
        print("登录成功")
        break
    else:
        count -= 1
        print( f"输入错误，还剩下{count}次机会")