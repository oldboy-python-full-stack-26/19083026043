# 1 定义宠物类（ Pet ），猫类（Cat）和狗类（Dog）宠物都有属性姓名（name）和年龄(age)宠物都有吃（eat）、喝（drink）、叫（shout）的方法
# 猫除了具有宠物类的方法，还有爬树（ climbTree ）的方法狗除了具有宠物类的方法，还有警戒（ police）的方法
# class Pet:
#     def __init__(self,name,age):
#         self.name = name
#         self.age = age
#
#     def eat(self):
#         print("吃")
#     def drink(self):
#         print("喝")
#     def shout(self):
#         print("叫")
#
# class Cat(Pet):
#     def climbTree(self):
#         print("猫上树")
#
# class Dog(Pet):
#     def police(self):
#         print("汪汪汪")
#
# mao = Cat("tom",4)
# gou = Dog("曼联",3)
# mao.eat()
# gou.shout()
# mao.climbTree()
# 2 建立一个汽车类Auto，包括轮胎个数，汽车颜色，车身重量，速度等属性，至少要求 汽车能够加速 减速 停车。
# 再定义一个小汽车类CarAuto 继承Auto 并添加空调、CD属性，并且重新实现方法覆盖加速、减速的方法
# class Auto:
#     def __init__(self, tire_num, color, weight, speed):
#         self.tire_num = tire_num
#         self.color = color
#         self.weight = weight
#         self.speed = speed
#     def accelerate(self):
#         self.speed += 20
#         print(f"车速增加了20码，此时车速为：{self.speed}码")
#     def decelerate(self):
#         self.speed -= 20
#         print(f"车速减少了20码，此时车速为：{self.speed}码")
#     def stop(self):
#         self.speed = 0
#         print("车已停稳")
#
# class CarAuto(Auto):
#     def __init__(self, tire_num, color, weight, speed, air_conditioner, cd):
#         super().__init__(tire_num, color, weight, speed)
#         self.air_conditioner = air_conditioner
#         self.cd = cd
#     def accelerate(self):
#         self.speed += 10
#         print(f"车速增加了10码，此时车速为：{self.speed}码")
#     def decelerate(self):
#         self.speed -= 10
#         print(f"车速减少了10码，此时车速为：{self.speed}码")
#
# benz = CarAuto(4, "red", 2, 60, "高档空调", "柏林之声")
# benz.accelerate()
# benz.decelerate()
# benz.stop()


# 3 银行卡类（BankCard）有余额（balance）属性和存款（deposit）取款（draw）的方法，只要取款金额大于余额即可完成取款操作
# 信用卡类（CreditCard）继承自银行卡类，信用卡多了透支额度（overdraft）属性，如果卡中余额和透支额度的和大于取款金额即可完成取款。如果透支，显示透支金额
#
# class BankCard(object):
#     def __init__(self,balance):
#         self.balance = balance
#     def dep(self,num):
#         print("正在进行存款操作")
#         self.balance += num
#     def draw(self,num):
#         if num <= self.balance:
#             print("正在取款")
#             self.balance -= num
#         else:
#             print("余额不足")
#
# class CreditCard(BankCard):
#     def __init__(self,bal):
#         self.overdraft = 10000
#         super(CreditCard,self).__init__(bal)

# 4 编写程序, A 继承了 B, 两个类都实现了 handle 方法, 在 A 中的 handle 方法中调用 B 的 handle 方法
# class B:
#     def handle(self):
#         pass
#
# class A(B):
#     def handle(self):
#         super().handle()



