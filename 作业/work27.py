# 1，写一个程序，线程C在线程B后执行，线程B在线程A之后进行
# from threading import Thread,Lock
# def run1():
#     while True:
#         if a.acquire():
#             print("A")
#             b.release()
#
# def run2():
#     while True:
#         if b.acquire():
#             print("B")
#             c.release()
#
# def run3():
#     while True:
#         if c.acquire():
#             print("C")
#             a.release()
# a = Lock()
# b = Lock()
# b.acquire()
# c = Lock()
# c.acquire()
# A = Thread(target=run1)
# B = Thread(target=run2)
# C = Thread(target=run3)
# A.start()
# B.start()
# C.start()
# 2，编写一个程序，开启3个线程，这3个线程的name分别为A、B、C，每个线程将自己的name在屏幕上打印10遍，要求输出结果必须按ABC的顺序显示；如：ABCABC….依次递推
# from threading import Thread,Lock
# def run():
#     for i in range(10):
#         if a.acquire():
#             print(A.name)
#             b.release()
#
# def run1():
#     for i in range(10):
#         if b.acquire():
#             print(B.name)
#             c.release()
#
# def run2():
#     for i in range(10):
#         if c.acquire():
#             print(C.name)
#             a.release()
#
# a = Lock()
# b = Lock()
# b.acquire()
# c = Lock()
# c.acquire()
# A = Thread(target=run,name= "A")
# B = Thread(target=run1,name= "B")
# C = Thread(target=run2,name="C")
# A.start()
# B.start()
# C.start()
# 3，简述生产者与消费者模式（用自己的话默写）
# 参考建议：生产者与消费者模式是通过一个容器来解决生产者与消费者的强耦合关系，
# 生产者与消费者之间不直接进行通讯，而是利用阻塞队列来进行通讯，生产者生成数据后直接丢给阻塞队列，
# 消费者需要数据则从阻塞队列获取，实际应用中，
# 生产者与消费者模式则主要解决生产者与消费者生产与消费的速率不一致的问题，
# 达到平衡生产者与消费者的处理能力，而阻塞队列则相当于缓冲区。
# 两个容器中,一个生产,一个吃,两者不能通信若是,效率不一致,就浪费了时间 资源,生成,且不容易维护.生成一个仓库,通过仓库中转
# 生产者生产完数据之后不用等待消费者处理，直接扔给阻塞队列，消费者不找生产者要数据，而是直接从阻塞队列里取，阻塞队列就相当于一个缓冲区，平衡了生产者和消费者的处理能力。