# 1，简述线程和协程的异同?
# ⽐线程更⼩的执⾏单元（微线程）
# ⼀个线程作为⼀个容器⾥⾯可以放置多个协程
# 进程线程的任务切换是由操作系统自行切换的，你自己不能控制
# 协程可以通过自己的程序（代码）来进行切换，自己能够控制
# 2，什么是并行，什么是并发？
# 并行是指两个或者多个事件在同一时刻发生；而并发是指两个或多个事件在同一时间间隔发生。
# 3，请解释同步和异步这两个概念？
# 同步调⽤：确定调用的顺序
# 提交一个任务,自任务开始运行直到此任务结束,我再提交下一个任务
# 异步调⽤：不确定顺序
# 一次提交多个任务,然后我就直接执行下一行代码
# 4，GIL锁是怎么回事?
# GIL本质就是一把互斥锁，既然是互斥锁，所有互斥锁的本质都一样，都是将并发运行变成串行，以此来控制同一时间内共享数据只能被一个任务所修改，进而保证数据安全。
# 可以肯定的一点是：保护不同的数据的安全，就应该加不同的锁。
# 　　要想了解GIL，首先确定一点：每次执行python程序，都会产生一个独立的进程。例如python test.py，python aaa.py，python bbb.py会产生3个不同的python进程
# 5，什么叫死锁？如何产生？如何解决
# 在线程间共享多个资源的时候， 如果两个线程分别占有⼀部分资源并且同时等待对⽅的资源， 就会造成死锁
# 死锁问题很大一部分是由于线程同时获取多个锁造成的
# 解决方法，递归锁，在Python中为了支持在同一线程中多次请求同一资源，python提供了可重入锁RLock。
# 这个RLock内部维护着一个Lock和一个counter变量，counter记录了acquire的次数，从而使得资源可以被多次require。
# 直到一个线程所有的acquire都被release，其他的线程才能获得资源。上面的例子如果使用RLock代替Lock，则不会发生死锁：
# 6，写一个程序，利用queue实现进程间通信；
# from multiprocessing import Queue, Process
# import time
#
# def write(q):
#     for value in ["a","b","c"]:
#         print("开始写入：",value)
#         q.put(value)
#         time.sleep(1)
#
# def read(q):
#     while True:
#         if not q.empty():
#             print("读取到的是",q.get())
#             time.sleep(1)
#         else:
#             break
# if __name__ == "__main__":
#     q = Queue()
#     pw = Process(target=write, args=(q,))
#     pr = Process(target=read, args=(q,))
#     pw.start()
#     pw.join()#等待接收完毕
#     pr.start()
#     pr.join()
#     print("接收完毕！")

# 7，写一个程序，包含十个线程，同时只能有五个子线程执行
# import threading
# from time import sleep
# from random import randint
#
# def run():
#     sp.acquire()
#     print(threading.current_thread().name)
#     sleep(randint(1,3))
#     sp.release()
#
# sp = threading.Semaphore(5)
# for i in range(1,11):
#     t = threading.Thread(target=run,name=f"线程{i}")
#     t.start()