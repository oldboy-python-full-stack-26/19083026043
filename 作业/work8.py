# 1.有如下文件，a1.txt，里面的内容为：
# 老男孩是最好的学校，
# 全心全意为学生服务，
# 只为学生未来，不为牟利。
# 我说的都是真的。哈哈
# 分别完成以下的功能：
# a,将原文件全部读出来并打印。
# f = open("a1.txt",mode="r",encoding="utf_8")
# print(f.readlines())
# b,在原文件后面追加一行内容：信不信由你，反正我信了。
# f = open("test","a1",encoding="utf-8")
# f.write("信不信由你，反正我信了。")
# c,将原文件全部读出来，并在后面添加一行内容：信不信由你，反正我信了。
# f.write("信不信由你，反正我信了。")
# f.seek(0,0)   # 移动光标   移动到文件的头部
# print(f.read())
# d,将原文件全部清空，换成下面的内容：
# 每天坚持一点，
# 每天努力一点，
# 每天多思考一点，
# 慢慢你会发现，
# 你的进步越来越大。
"""
f = open("test","w+",encoding="utf-8")
f.write("每天坚持一点，
每天努力一点，
每天多思考一点，
慢慢你会发现，
你的进步越来越大。")
"""
# f.seek(0,0)   # 移动光标   移动到文件的头部
# print(f.read())
# 2.有如下文件，t1.txt,里面的内容为：
# 葫芦娃，葫芦娃，
# 一根藤上七个瓜
# 风吹雨打，都不怕，
# 啦啦啦啦。
# 我可以算命，而且算的特别准:
# 上面的内容你肯定是心里默唱出来的，对不对？哈哈
# 分别完成下面的功能：
# a,以r的模式打开原文件，利用for循环遍历文件句柄。
# f = open("t1.txt",mode="r",encoding="utf-8")
# for i in f:
#     print(i)
# b,以r的模式打开原文件，以readlines()方法读取出来，并循环遍历 readlines(),并分析a,与b 有什么区别？深入理解文件句柄与 readlines()结果的区别。
# f = open("t1.txt",mode="r",encoding="utf-8")
# for i in f.readlines():
#     print(i)
# 分析：b,a结果一样的。
# 深入理解：文件句柄 -- 操作文件使用的变量名，而readlines()只是一个操作方法
# c,以r模式读取‘葫芦娃，’前四个字符。
# f = open("t1.txt",mode="r",encoding="utf-8")
# f.seek(0,0)
# print(f.read(4))
# f.close()
# d,以r模式读取第一行内容，并去除此行前后的空格，制表符，换行符。
# f = open("t1.txt",mode="r",encoding="utf_8")
# print(f.readline().strip())
# e,以a+模式打开文件，先追加一行：‘老男孩教育’然后在从最开始将 原内容全部读取出来。
# f = open("t1.txt",mode="a+",encoding="utf_8")
# f.seek(0,2)
# f.write("老男孩教育")
# f.seek(0,0)
# print(f.readlines())
# 3.文件a.txt内容：每一行内容分别为商品名字，价钱，个数。
# apple 10 3
# tesla 100000 1
# mac 3000 2
# lenovo 30000 3
# chicken 10 3
# 通过代码，将其构建成这种数据类型：[{'name':'apple','price':10,'amount':3},{'name':'tesla','price':1000000,'amount':1}......] 并计算出总价钱。
#
# f = open("a.txt",mode="r",encoding="utf-8")
# lst=[]
# sum = 0
# for li in f:
#     dic={}
#     a,b,c= i.strip().split(" ")
#     lst.append({"name":a,'price':b,'amount':c,})
#     sum += int(b) * int(c)
# print(lst)
# print("总价钱为:",sum)
# 4.有如下文件：
# alex是老男孩python发起人，创建人。
# alex其实是人妖。
# 谁说alex是sb？
# 你们真逗，alex再牛逼，也掩饰不住资深屌丝的气质。
# 将文件中所有的alex都替换成大写的SB（文件的改的操作）。
# with open("test","r",encoding="utf-8")as f,\
#     open("test2","w",encoding="utf-8")as f1:
#     for i in f:
#         f1.write(i.replace("alex","SB"))
#         f1.flush()
# 5.文件a1.txt内容(选做题)
#
# name:apple price:10 amount:3 year:2012
# name:tesla price:100000 amount:1 year:2013
# .......
#
# 通过代码，将其构建成这种数据类型：
# [{'name':'apple','price':10,'amount':3,year:2012},
# {'name':'tesla','price':1000000,'amount':1}......]
# 并计算出总价钱。
#
# 6.文件a1.txt内容(选做题)
# 序号 部门 人数 平均年龄 备注
# 1 python 30 26 单身狗
# 2 Linux 26 30 没对象
# 3 运营部 20 24 女生多
# .......
#
# 通过代码，将其构建成这种数据类型：
# [{'序号':'1','部门':Python,'人数':30,'平均年龄':26,'备注':'单身狗'},