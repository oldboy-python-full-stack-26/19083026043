# 1.看代码写结果
#
# v1 = [1,2,3,4,5]
# v2 = [v1,v1,v1]
# v1.append(6)
# print(v1)
# print(v2)
# [1, 2, 3, 4, 5, 6]
# [[1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6], [1, 2, 3, 4, 5, 6]]
# 2.看代码写结果
#
# v1 = [1,2,3,4,5]
# v2 = [v1,v1,v1]
# v2[1][0] = 111
# v2[2][0] = 222
# print(v1)
# print(v2)
# [222, 2, 3, 4, 5]
# [[222, 2, 3, 4, 5], [222, 2, 3, 4, 5], [222, 2, 3, 4, 5]]
# 3.看代码写结果，并解释每一步的流程。
#
# v1 = [1,2,3,4,5,6,7,8,9]
# v2 = {}
# for item in v1:
#     if item < 6:    <6,跳出循环
#         continue
#     if 'k1' in v2:
#         v2['k1'].append(item)  追加
#     else:
#         v2['k1'] = [item ]
# print(v2)
# {'k1': [6, 7, 8, 9]}
# 4.简述赋值和深浅拷贝？
"""
赋值: 多个变量名指向同一个内存地址
浅拷贝:只拷贝第一层的内存地址,咱们能看到是因为通过内存地址去查找值然后显示的
深拷贝:不可变共用,可变数据类型开辟新的空间,不管嵌套多少层都是这样的原理
"""
# 5.看代码写结果
#
# import copy
# v1 = "alex"
# v2 = copy.copy(v1)
# v3 = copy.deepcopy(v1)
# print(v1 is v2)
# print(v1 is v3)
# True
# True
# 6.看代码写结果
#
# import copy
# v1 = [1,2,3,4,5]
# v2 = copy.copy(v1)
# v3 = copy.deepcopy(v1)
# print(v1 is v2)
# print(v1 is v3)
# False
# False
# 7.看代码写结果
#
# import copy
# v1 = [1,2,3,4,5]
# v2 = copy.copy(v1)
# v3 = copy.deepcopy(v1)
# print(v1[0] is v2[0])
# print(v1[0] is v3[0])
# print(v2[0] is v3[0])
# Ture
# True
# True
# 8.看代码写结果
#
# import copy
# v1 = [1,2,3,4,[11,22]]
# v2 = copy.copy(v1)
# v3 = copy.deepcopy(v1)
# print(v1[-1] is v2[-1])
# print(v1[-1] is v3[-1])
# print(v2[-1] is v3[-1])
# Ture
# False
# False
# 9.看代码写结果
#
# import copy
# v1 = [1,2,3,{"name":'太白',"numbers":[7,77,88]},4,5]
# v2 = copy.copy(v1)
# print(v1 is v2)
#
# print(v1[0] is v2[0])
# print(v1[3] is v2[3])
# print(v1[3]['name'] is v2[3]['name'])
# print(v1[3]['numbers'] is v2[3]['numbers'])
# print(v1[3]['numbers'][1] is v2[3]['numbers'][1])
# False
# True
# True
# True
# True
# True



# 10.看代码写结果
#
# import copy
# v1 = [1,2,3,{"name":'太白',"numbers":[7,77,88]},4,5]
# v2 = copy.deepcopy(v1)
# print(v1 is v2)
# print(v1[0] is v2[0])
# print(v1[3] is v2[3])
#
# print(v1[3]['name'] is v2[3]['name'])
# print(v1[3]['numbers'] is v2[3]['numbers'])
# print(v1[3]['numbers'][1] is v2[3]['numbers'][1])
# False
# True
# False
# True
# False
# True

# 11.请说出下面a,b,c三个变量的数据类型。
# a = ('太白金星')字符串
# b = (1,)元组
# c = ({'name': 'barry'})字
#
# 按照需求为列表排序：
# l1 = [1, 3, 6, 7, 9, 8, 5, 4, 2]
# # 从大到小排序
# l1.sort()
# # 从小到大排序
# l1.sort(reverse=True)
# # 反转l1列表
# l1（：：-1）
# 13.利用python代码构建一个这样的列表(升级题)：
#
# [['_','_','_'],['_','_','_'],['_','_','_']]
# 14.看代码写结果：
#
# l1 = [1,2,]
# l1 += [3,4]
# print(l1)
# [1，2，3，4]
# 15.看代码写结果：
# dic = dict.fromkeys('abc',[])
# dic['a'].append(666)
# print(dic)
# {'a': [666], 'b': [666], 'c': [666]}
# 16.l1 = [11, 22, 33, 44, 55]，请把索引为奇数对应的元素删除
#
# 17.dic = {'k1':'太白','k2':'barry','k3': '白白', 'age': 18} 请将字典中所有键带k元素的键值对删除.
#
# 18.完成下列需求：
# s1 = '宝元'

# 将s1转换成utf-8的bytes类型。
# print(s1.encode("Utf-8"))
# 将s1转化成gbk的bytes类型。
# print(s1.encode("gbk"))
# b = b'\xe5\xae\x9d\xe5\x85\x83\xe6\x9c\x80\xe5\xb8\x85'
# b为utf-8的bytes类型，请转换成gbk的bytes类型。
# b1 = b.decode("utf-8")
# print(b1.encode("gbk"))
# 19.用户输入一个数字，判断一个数是否是水仙花数。
# 水仙花数是一个三位数, 三位数的每一位的三次方的和还等于这个数. 那这个数就是一个水仙花数,
# 例如: 153 = 1**3 + 5**3 + 3**3
# num = int(input("请输入一个水仙花数字："))
# for a in range(1,10):    #定义百位数取值区间 (1,9)
#     for b in range(10):    #定义十位数取值区间 (0,9)
#         for c in range(10):    #定义个位数取值区间 (0,9)
#             num = a * 100 + b * 10 + c * 1    #水仙花数组成定义
#             if num  < 1000:    #水仙花数取值区间 [100,1000)
#                 if num == a ** 3 + b ** 3 + c ** 3:     #水仙花数判定条件
#                     print("是水仙花数")
#                 else:
#                     print("不是水仙花数")
#
# 20.把列表中所有姓周的⼈的信息删掉(此题有坑, 请慎重):
# lst = ['周⽼⼆', '周星星', '麻花藤', '周扒⽪']
# 结果: lst = ['麻花藤']
# lst = ['周⽼⼆', '周星星', '麻花藤', '周扒⽪']
# lst1 = lst.copy()
# for i in lst1:
#     if "周" in i:
#         lst.remove(i)
# print(lst)
# 21.车牌区域划分, 现给出以下车牌. 根据车牌的信息, 分析出各省的车牌持有量. (选做题)
# cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041']
# locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南'}
# 结果: {'⿊⻰江':2, '⼭东': 2, '上海': 1}
# cars = ['鲁A32444','鲁B12333','京B8989M','⿊C49678','⿊C46555','沪 B25041']
# locals = {'沪':'上海', '⿊':'⿊⻰江', '鲁':'⼭东', '鄂':'湖北', '湘':'湖南'}
# dit = {}
# car = []
# for i in locals:
#     count = 0
#     for j in cars:
#         if j.startswith(i):
#             count += 1
#         dit[locals[i]] = count
# del dit['湖南']
# del dit['湖北']
# print(dit)