

# 2.写函数，检查获取传入列表或元组对象的所有奇数位索引对应的元素，并将其作为新列表返回给调用者。
# def func(args):
#     lst = []
#     for i in range(len(args)):
#         if i % 2 == 1:
#             lst.append(args[i])
#     return lst

# 3.写函数，判断用户传入的对象（字符串、列表、元组）长度是否大于5。
# def s_len(args):
#     if len(args) > 5:
#         print(True)
#     else:
#         print(False)
# s_len('ssssssssssssss')
# 4.写函数，检查传入列表的长度，如果大于2，那么仅保留前两个长度的内容，并将新内容返回给调用者。
# def func(args):
#     if len(args) > 2:
#         args = args[:2]
#         return args
#     else:
#         return'长度小于2'
# print(func([1,2,3,4,]))


# 5.写函数，计算传入函数的字符串中,[数字]、[字母和中文]以及 [其他]的个数，并返回结果。
# def s_obj(obj):
#     num_count = 0
#     str_count = 0
#     other_count = 0
#     for i in obj:
#         if i.isdecimal():
#             num_count += 1
#         elif i.isalpha():
#             str_count += 1
#         else:
#             other_count += 1
#     dic = {"num":num_count,"str":str_count,"other":other_count}
#     return dic
# print(s_obj("ssssssssss333;l"))

# 6.写函数，接收两个数字参数，返回比较大的那个数字。
# def my_max(num1,num2):
#     return max(num1,num2)
#
# print(my_max(1,3))

# 7.写函数，检查传入字典的每一个value的长度,如果大于2，那么仅保留前两个长度的内容，并将新内容返回给调用者。
# dic = {"k1": "v1v1", "k2": [11,22,33,44]}
# PS:字典中的value只能是字符串或列表
# def dic_long(dic):
#     for k, v in dic.items():
#         if len(v) > 2:
#             dic[k] = v[0:2]
#     return dic
#
# print(dic_long(dic))
# 8.写函数，此函数只接收一个参数且此参数必须是列表数据类型，此函数完成的功能是返回给调用者一个字典，此字典的键值对为此列表的索引及对应的元素。例如传入的列表为：[11,22,33] 返回的字典为 {0:11,1:22,2:33}。
# def lst_2(lst):
#     dic = {}
#     for i in lst:
#         dic.update({lst.index(i):i})
#     return dic
#
# print(lst_2([11,22,33]))
# 9.写函数，函数接收四个参数分别是：姓名，性别，年龄，学历。用户通过输入这四个内容，然后将这四个内容传入到函数中，此函数接收到这四个内容，将内容追加到一个student_msg文件中。
# def func(name,sex,age,edu):
#     with open("student_msg","a",encoding="utf-8") as f:
#         f.write(f"{name},{sex},{age},{edu}")
# func("sg","男",24,"bk")
# 10.对第9题升级：支持用户持续输入，Q或者q退出，性别默认为男，如果遇到女学生，则把性别输入女。
# def func(name,age,edu,sex="男"):
#     with open("student_msg", "a", encoding="utf-8") as f:
#         f.write(f"{name},{sex},{age},{sc}")
# while True:
#     info = input("请输入信息(姓名,年龄,学历,性别):")
#     if info == "q" or info == "Q":
#         break
#     else:
#         j = info.split(",")
#         func(j[0],j[1],j[2],j[3])
#  写函数，用户传入修改的文件名，与要修改的内容，执行函数，完成整个文件的批量修改操作（选做题）。
# def func(file_name,opretion):
#     with open(file_name,"r",encoding="utf-8") as f, \
#         open("file","w",encoding="utf-8") as f1:
#         for i in f:
#             i = i.replace(opretion[0],opretion[1])
#             f1.write(i)
#         import os
#     os.rename(file_name,"file01")
#     os.rename("file",file_name)
# func("student_msg",["女","男"])
def func(foo):
    print(1)
    print("sss")
    print(2)

def foo(b):
    print(3)
    ss=b()
    print(4)

def f(a,b):
    a(b)

f(foo,func)