#2
name = input(">>>")
print(type(name))
#3
age = 19
sex = "男"
if age > 18:
    print("ok")
#二选一
if age < 18:
    print("未成年")
else:
    print("成年")
#多选一或零
if age > 25:
    print("大于25")
elif age > 22:
    print("大于22")
elif age > 18:
    print("大于18")

#多选一
if age > 25:
    print("大于25")
elif age > 22:
    print("大于22")
elif age > 19:
    print("大于19")
else:
    print("不详")

#多选多
if age > 18:
    print("大于18")
if age > 15:
    print("大于15")
if age > 12:
    print("大于12")

#if嵌套
if age > 18:
    if sex == "男":
        print("ok")

#4
print("""
文能提笔安天下,
武能上马定乾坤.
心存谋略何人胜,
古今英雄唯世君.""")

#5
answer = 66
while True:
    guess_number = int(input("请输入猜测结果："))
    if guess_number > answer:
        print("太大了")
        continue
    elif guess_number < answer:
        print("太小了")
        continue
    else:
        print("猜对了")
        break
#6ge = int(input("请输入年龄:"))
if age <=10:
    print("小屁孩")
if age > 10:
    if age <= 20:
        print("青春期叛逆的小屁孩")
if age > 20:
    if age <= 30:
        print("开始混社会小屁孩儿")
if age > 30:
    if age <= 40:
        print("看老老大不了,赶紧结婚小屁孩儿")
if age > 40:
    if age <= 50:
        print("家里有个不听话的小屁孩儿")
if age > 50:
    if age <= 60:
        print("自己马上变成不听话的老屁孩儿")
if age > 60:
    if age <= 70:
        print("活着还不错的老屁孩儿")
if age > 70:
    if age < 90:
        print("人生就快结束了的一个老屁孩儿")
if age > 90:
        print("再见了这个世界")

#7
 单行注释
#"""
 多行注释



#8
# python2:
源码不统一
有重复代码
整型的除法:整数
不加括号也可以
input() 输入什么类型就是什么类型
raw_input() 获取到的都是字符串


# python3:
源码统一
没有重复代码
整型的除法:浮点数(小数)
print()
input 获取到的都是字符串

#9
username = input("请输入用户名：")
if username == "麻花藤":
    print("真聪明")
else:
    print("输入错误")

#10
month = int(input("请输入月份:"))
if month == 1:
    print("一月,香肠")
elif month == 2:
    print("二月,鸡")
elif month == 3:
    print("三月,鱼")
elif month == 4:
    print("四月,牛")
elif month == 5:
    print("五月,羊")
elif month == 6:
    print("六月,鸭")
elif month == 7:
    print("七月,狗肉")
elif month == 8:
    print("八月,猪蹄")
elif month == 9:
    print("九月,撸串儿")
elif month == 10:
    print("十月,月饼")
elif month == 11:
    print("十一月,鹅肉")
elif month == 12:
    print("十二月,腊肉")
else:
    print("done")

#11
num = int(input("请输入数字:"))
if num==90:
    print("A")
elif num==80:
    print("B")
elif num==70:
    print("C")
elif num==60:
    print("D")
else:
    print("不及格")